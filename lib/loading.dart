// ignore_for_file: prefer_const_constructors
import 'dart:js';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:load_animation/pages/login_home.dart';

class LoadingPage extends StatefulWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  void initState() {
    super.initState();
    _gotoLogin();
  }

  _gotoLogin() async {
    await Future.delayed(Duration(milliseconds: 2000), () {});
    Navigator.pushReplacement(
        this.context, MaterialPageRoute(builder: (context) => HomeLogin()));
  }

  // create or build the animation with flutter_spinkit_animations
  final spinkitCircleLoader = SpinKitFadingCircle(
    itemBuilder: ((context, index) => DecoratedBox(
          decoration: BoxDecoration(
            color: index.isEven ? Colors.deepPurple : Colors.deepPurple,
          ),
        )),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      body: Center(child: spinkitCircleLoader),
    );
  }
}
