import 'package:flutter/material.dart';
import 'package:load_animation/loading.dart';

class HomeLogin extends StatefulWidget {
  const HomeLogin({Key? key}) : super(key: key);

  @override
  State<HomeLogin> createState() => _HomeLoginState();
}

class _HomeLoginState extends State<HomeLogin> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  // future async function

  // dispose the controller after usage
  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'LOGIN',
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
            letterSpacing: 2.0,
          ),
        ),
        centerTitle: true,
        elevation: 0,
        // backgroundColor: Colors.grey[900],
      ),
      // END OF APPBAR
      body: SafeArea(
        // center all of the content going into our safeArea with a Center widget
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 25.0),
              // User Hello text
              const Text(
                'PLEASE LOGIN!',
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  // color: Colors.white,
                  fontSize: 36,
                  letterSpacing: 2.0,
                ),
              ),

              const SizedBox(height: 10),
              // An intro text or message to the user
              const Text(
                'Welcome back, your account is ready!',
                style: const TextStyle(
                  fontSize: 20,
                ),
              ),
              const SizedBox(height: 50),

              // User email textfield
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  // wrapping textfield in a padding and give it a box decoration
                  // box decoration controlls the border lines and edges/curves
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    // email for the user goes below
                    child: TextField(
                      controller: _passwordController,
                      decoration: const InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: 'Email'),
                    ),
                  ),
                ),
              ),
              // END OF USERNAME TEXT FIELD PADDING
              // little height space between the text fields
              const SizedBox(height: 20),
              // User password textfield
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  // wrapping textfield in a padding and give it a box decoration
                  // box decoration controlls the border lines and edges/curves
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    // password for the user goes below
                    // NB* not linked to any database
                    child: TextField(
                      controller: _passwordController,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Password',
                      ),
                    ),
                  ),
                ),
              ),
              // END OF PASSWORD TEXT FIELD PADDING
              const SizedBox(height: 25),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Not a member?',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const Text(
                    'Register Now!',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),

              // wrap a floating action btn in a padding to remove it from the edge of screen
              // aligned the button in the center
              Padding(
                padding: const EdgeInsets.all(40.0),
                child: Center(
                  // button simply routes to the home page/dashboard of our app onClick
                  child: FloatingActionButton(
                    onPressed: () {
                      /*
                        floating button in the login, onClicked
                        immediately loads the splash screen 
                        re-routes to the profile content page
                       */
                      Navigator.pushReplacement(
                          this.context,
                          MaterialPageRoute(
                              // navigates to the easy made profile content page
                              builder: (context) => const LoadingPage()));
                    },
                    child: const Icon(
                      Icons.login_rounded,
                      size: 25,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
