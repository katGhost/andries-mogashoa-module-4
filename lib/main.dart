import 'package:flutter/material.dart';
import 'package:load_animation/loading.dart';
import 'package:load_animation/pages/login_home.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'a simple loading animation/splash screen',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        scaffoldBackgroundColor: Colors.black87,
        textTheme: const TextTheme(
            bodyText2: TextStyle(
                color: Colors.grey, fontFamily: 'fonts/RobotoCondensed')),
      ),
      home: const LoadingPage(),
    );
  }
}
